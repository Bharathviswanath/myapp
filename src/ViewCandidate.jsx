import React, { Component } from "react";
//import { ReactComponent } from "*.svg";
import axios from "axios";
import swal from "sweetalert";
import { Link } from "react-router-dom";
class ViewCandidate extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      data2: [],
      isLoaded: false
    };
  }

  componentDidMount() {
    fetch("http://localhost:8080/candidate")
      .then(Response => Response.json())
      .then(json => {
        this.setState({
          isLoaded: true,
          data: json,
          data2: json
        });
      });
  }

  onDelete(event, id) {
    event.preventDefault();
    console.log(id);
    swal({
      title: "Confirmation to Delete????",
      text: "if u proceed,u cant get it again",
      icon: "warning",
      buttons: true
    }).then(del => {
      if (del) {
        axios.delete("http://localhost:8080/candidate/" + id).then(res => {
          if (res.status === 200) {
            swal({
              title: "Successful Deletion",
              text: "you have successfully deleted",

              icon: "success",
              buttons: true
            }).then(res => {
              window.location.reload();
            });
          }
        });
      } else {
        swal({
          title: "Delete request declined",
          icon: "info"
        });
      }
    });
    this.props.history.push("/");
    this.props.history.push("/view Candidate");
  }
  filterCandidate = event => {
    let candiSearch = [];
    candiSearch = this.state.data.filter(c => {
      return c.aadharNo.toString().includes(event.target.value);
    });
    this.setState({
      data2: candiSearch
    });
  };
  onUpdate(event, id) {
    event.preventDefault();
  }

  render() {
    var { isLoaded, data2 } = this.state;
    if (!isLoaded) {
      return <div>loading......</div>;
    } else {
      // let s = "/candidate/candidateEdit/" + data.candidate.aadharNo;
      // console.log(s);
      return (
        <div class="toolbar">
          <div class="d-flex justify-content-center h-100">
            <div class="searchbar">
              <input
                class="search_input"
                type="text"
                placeholder="Search by aadhar number"
                onChange={this.filterCandidate}
              />
              <a href="#" class="search_icon">
                <i class="fas fa-search"></i>
              </a>
            </div>
          </div>

          <table class="table table-bordered table-striped">
            <th data-field="aadharNo">AadharNo</th>
            <th data-field="firstName">FirstName</th>
            {/* <th data-field="surName">SurName</th> */}
            <th data-field="phoneNumber">PhoneNumber</th>
            {/* <th data-field="alternateNumber">AlternateNumber</th> */}
            <th data-field="email">Email</th>
            {/* <th data-field="tEx">Total Exp</th>
                <th data-field="rEx">Revelent Exp</th> */}
            {/* <th data-field="primarySkills">PrimarySkills</th> */}
            {/* <th data-field="secondarySkills">SecondarySkills</th> */}
            <th data-field="qualification">Qualification</th>
            {/* <th data-field="additionalQualification">Add Qualification</th> */}
            {/* <th data-field="expectedCTC">ExpectedCTC</th> */}
            {/* <th data-field="currentCTC">CurrentCTC</th> */}
            <th data-field="positionApplied">PositionApplied</th>
            <th data-field="currentAddress">Curr Address</th>
            {/* <th data-field="permanentAddress">Perm Address</th> */}
            <th data-field="gender">Gender</th>
            {/* <th data-field="date">Date</th> */}
            <th
              data-field="actions"
              data-formatter="operateFormatter"
              data-events="operateEvents"
            >
              Actions
            </th>

            {data2.map(data => (
              <tr>
                <td>{data.aadharNo}</td>
                <td>{data.firstName}</td>
                {/* <td>{data.surName}</td> */}
                <td>{data.phoneNumber}</td>
                {/* <td>{data.alternateNumber}</td> */}
                <td>{data.email}</td>
                {/* <td>{data.tEx}</td>
                    <td>{data.rEx}</td> */}
                {/* <td>{data.primarySkills}</td> */}
                {/* <td>{data.secondarySkills}</td> */}
                <td>{data.qualification}</td>
                {/* <td>{data.additionalQualification}</td> */}
                {/* <td>{data.expectedCTC}</td> */}
                {/* <td>{data.currentCTC}</td> */}
                <td>{data.positionApplied}</td>
                <td>{data.currentAddress}</td>
                {/* <td>{data.permanentAddress}</td> */}
                <td>{data.gender}</td>
                {/* <td>{data.date}</td> */}
                <a href="#">
                  <span
                    className="glyphicon glyphicon-trash"
                    onClick={event => this.onDelete(event, data.aadharNo)}
                  >
                    Delete
                  </span>
                </a>
                <Link to={"candidateEdit/" + data.aadharNo}>
                  <a href="#">
                    <span className="glyphicon glyphicon-pencil">Edit</span>
                  </a>
                </Link>
              </tr>
            ))}
            <br></br>
          </table>
        </div>
      );
    }
  }
}

export default ViewCandidate;
