import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

export default function StepThree() {
  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Other Details
      </Typography>

      <Grid container spacing={3}>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="total experience"
            name="total experience"
            label="total experience"
            fullWidth
            // autoComplete="fname"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="relavent experience"
            name="relavent experience"
            label="relevent experience"
            fullWidth
            //  autoComplete="lname"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            required
            id="current CTC"
            name="current CTC"
            label="current CTC"
            fullWidth
            // autoComplete="billing address-level2"
          />
        </Grid>
        <Grid item xs={12} sm={6}>
          <TextField
            id="expected CTC"
            name="expected CTC"
            label="expected CTC"
            fullWidth
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            required
            id="email"
            name="email"
            label="email"
            fullWidth
            //autoComplete="billing address-line1"
          />
        </Grid>

        <Grid item xs={12}>
          <TextField
            id="position applied"
            name="position applied"
            label="position applied"
            fullWidth
            // autoComplete="billing address-line2"
          />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
