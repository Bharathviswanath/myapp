import React from "react";
import { Nav, Navbar } from "react-bootstrap";
import styled from "styled-components";

const Styles = styled.div`
  .navbar {
    background-color: #333;
  }

  .navbar-brand,
  .navbar-nav .nav.link {
    color: #aaa;

    &:hover {
      color: white;
    }
  }
`;

export const NavigationBar = () => (
  <Styles>
    <Navbar expand="lg">
      <Navbar.Brand href="/candidate">Candidate</Navbar.Brand>
      <Navbar.Brand href="/interview">Interview</Navbar.Brand>
      <Navbar.Brand href="/view candidate">ViewCandidate</Navbar.Brand>
      <Navbar.Brand href="/view interview">ViewInterview</Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <nav className="ml-auto">
          <nav class="nav flex-column">
            <Nav.Item>
              <Nav.Link href="/">Bassure Solution Private Limited</Nav.Link>
            </Nav.Item>
          </nav>
        </nav>
      </Navbar.Collapse>
    </Navbar>
  </Styles>
);
