<div class="form-group">
                        <label for="selectDate">Date of Birth:</label>
                        <select id="selectDate" style="width:auto;" class="form-control selectWidth">
                            @for ($i = 1; $i < = 31; $i++)
                            <option class="">{{$i}}</option>
                            @endfor
                        </select>
                        <select id="selectMonth" style="width:auto;" class="form-control selectWidth">
                            @for ($i = 1; $i < = 12; $i++)
                            <option class="">{{$i}}</option>
                            @endfor
                        </select>
                        <select id="selectYear" style="width:auto;" class="form-control selectWidth">
                            @for ($i = 1900; $i <= 2050; $i++)
                            <option class="">{{$i}}</option>
                            @endfor
                        </select>
                    </div>