import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Candidate } from "./Candidate";
import { Interview } from "./Interview";
import { Home } from "./Home";
import { Login } from "./Login";
import { Layout } from "./components/Layout";
import { NavigationBar } from "./components/NavigationBar";
import ViewCandidate from "./ViewCandidate";
import { ViewInterview } from "./ViewInterview";
import CandidateForm from "./CandidateForm/CandidateForm";
import CandidateEdit from "./CandidateEdit";
import InterviewEdit from "./InterviewEdit";

function App() {
  return (
    <React.Fragment>
      <NavigationBar />
      <Layout>
        <Router>
          <Switch>
            <Route path="/candidate" component={Candidate} />
            <Route path="/interview" component={Interview} />
            <Route path="/view candidate" component={ViewCandidate} />
            <Route path="/view interview" component={ViewInterview} />
            <Route exact path="/" component={Home} />
            <Route
              exact
              path="/candidateEdit/:aadhar"
              component={CandidateEdit}
            />
            <Route
              exact
              path="/interviewEdit/:aadhar"
              component={InterviewEdit}
            />
          </Switch>
        </Router>
      </Layout>
    </React.Fragment>
  );
}

//import { Form } from 'react-bootstrap/lib/Navbar';

export default App;
