import React, { Component } from "react";

export class Home extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      <div className="row">
        <div class="col-md-6">
          <div class="img-fluid" alt="Responsive image">
            <img src="https://www.digitallschool.com/wp-content/uploads/2017/09/SmallLogo.png"></img>
            <h3>BAssure Solution Pvt Ltd.,</h3>
            <h6>shasaai Building, No.4/606, 2nd Floor, 1st Main Rd,</h6>
            <h6>Nehru Nagar, Kottivakkam, Chennai, Tamil Nadu 600041</h6>
          </div>
        </div>
      </div>
    );
  }
}
