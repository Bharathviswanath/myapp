import React, { Component } from "react";
import { Button } from "react-bootstrap";
import axios from "axios";
import swal from "sweetalert";
import { Link } from "react-router-dom";

export class ViewInterview extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      isLoaded: false
    };
  }

  componentDidMount() {
    axios.get("http://localhost:8080/interview").then(Response => {
      this.setState({
        data: Response.data,
        isLoaded: true
      });
      console.log(Response);
    });
  }

  onDelete(event, id, modeOfInterview) {
    event.preventDefault();
    console.log(id);
    swal({
      title: "Confirmation to Delete????",
      text: "if u proceed,u cant get it again",
      icon: "warning",
      buttons: true
    }).then(del => {
      if (del) {
        axios
          .delete(
            "http://localhost:8080/interview/" + id + "/" + modeOfInterview
          )
          .then(res => {
            if (res.status === 200) {
              swal({
                title: "Successful Deletion",
                text: "you have successfully deleted",

                icon: "success",
                buttons: true
              }).then(res => {
                window.location.reload();
              });
            }
          });
      } else {
        swal({
          title: "Delete request declined",
          icon: "info"
        });
      }
    });
    this.props.history.push("/viewInterview");
  }

  render() {
    var { isLoaded, data } = this.state;
    if (!isLoaded) {
      return <div>loading......</div>;
    } else {
      let intdata = data.map(datum => {
        let dat = datum;
        return (
          <div
            key={datum.candidate.aadharNo}
            className="card border-primary mb-3"
          >
            <div className="card-header">
              <img
                className="card-img-top"
                src="https://www.pinclipart.com/picdir/middle/116-1168985_male-icon-1stacy-dreher2017-08-01t16-business-clipart.png"
              ></img>
            </div>
            <img className="card-img-top" alt={datum.candidate.firstName} />
            <div className="card-body">
              <h5>
                <i>
                  {datum.candidate.firstName + " " + datum.candidate.surName}
                </i>
              </h5>
              <h5 className="card-title">
                {"Aadhar: " + datum.candidate.aadharNo}
              </h5>
              <p class="card-text">
                {"Skills: " +
                  datum.candidate.primarySkills +
                  " " +
                  datum.candidate.secondarySkills}
              </p>
              <p className="card-text">
                <b>modeOfInterview : </b>
                <i>{datum.modeOfInterview}</i>
              </p>
              <p className="card-text">
                <b>date : </b>
                <i>{datum.date}</i>
              </p>
              <p className="card-text">
                <b>location : </b>
                <i>{datum.location}</i>
              </p>
              <p className="card-text">
                <b>recommandation : </b>
                <i>{datum.recommandation}</i>
              </p>
              <p className="card-text">
                <b>interviewPanel : </b>
                <i>{datum.interviewPanel}</i>
              </p>
              <p className="card-text">
                <b>reference : </b>
                <i>{datum.reference}</i>
              </p>
              <div className="col-md-6">
                <a href="#">
                  <span
                    className="glyphicon glyphicon-trash"
                    value={datum.candidate.aadharNo}
                    onClick={event =>
                      this.onDelete(event, datum.candidate.aadharNo)
                    }
                  >
                    Delete
                  </span>
                </a>
              </div>
              <Link to={"interviewEdit/" + datum.candidate.aadharNo}>
                <a href="#">
                  <span className="glyphicon glyphicon-pencil">Edit</span>
                </a>
              </Link>
            </div>
          </div>
        );
      });

      return <div className="card-columns">{intdata}</div>;
    }
  }
}
