import React from "react";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import SaveIcon from "@material-ui/icons/Save";
import axios from "axios";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import swal from "sweetalert";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 100
  }
}));
class InterviewEdit extends React.Component {
  candidate = {
    aadharNo: "",
    firstName: "",
    surName: "",
    phoneNumber: "",
    alternateNumber: "",
    email: "",
    tEx: "",
    rEx: "",
    primarySkills: "",
    secondarySkills: "",
    qualification: "",
    additionalQualification: "",
    expectedCTC: 0,
    currentCTC: 0,
    positionApplied: "",
    currentAddress: "",
    permanentAddress: "",
    gender: "",
    date: ""
  };
  interview = {
    candidate: this.candidate,
    modeOfInterview: "",
    location: "",
    recommendation: "",
    interviewPanel: "",
    reference: "",
    date: ""
  };
  state = {
    interview: this.interview
  };
  handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    let inter = { ...this.state.interview };
    inter[name] = value;
    this.setState({
      interview: inter
    });
  };
  handleSubmit = event => {
    event.preventDefault();
    let interview = this.state.interview;
    swal({
      title: "Hey  Are you ready to Save?",
      text: "Make sure the above info is true!!!",
      icon: "warning",
      buttons: true,
      dangerMode: true
    }).then(submit => {
      if (submit) {
        axios
          .put("http://localhost:8080/interview", interview)
          .then(res => {
            console.log(res);
            res.status === 200
              ? swal({
                  title: "success,Form  Updation!!!!",
                  text: "yeah,your form is successfully submitted",
                  icon: "success",
                  buttons: true
                }).then(success => {
                  if (success) {
                    window.location.replace("http://localhost:3001/interview");
                  } else {
                    window.location.replace("http://localhost:3001/interview");
                  }
                })
              : swal({
                  title: "Failed,Form Submmission ,Updation!!!!",
                  text: "your form is not successfully updated",
                  icon: "error"
                });
          })
          .catch(res => {
            console.log(res);
            swal({
              title: "Exception",
              icon: "error"
            });
          });
      } else {
        swal({
          text: "you are not completing your form updattion",
          icon: "info"
        });
      }
    });
  };
  handleClear = event => {
    event.preventDefault();
    this.setState({
      interview: this.interview
    });
  };
  componentDidMount() {
    let aadhar = this.props.match.params.aadhar;
    console.log(aadhar);
    axios
      .get("http://localhost:8080/interview/" + aadhar)
      .then(res => {
        console.log(res.data);
        this.setState({
          interview: res.data
        });
      })
      .catch(res => console.log(res));
  }
  render() {
    return (
      <div className="container">
        <div className="bg-light">
          <h4>Edit Form</h4>
          <form
            onSubmit={this.handleSubmit}
            noValidate
            autoComplete="off"
            fullWidth
          >
            <TextField
              id="filled-secondary"
              label="Candidate"
              variant="outlined"
              color="secondary"
              name="candidate"
              value={this.state.interview.candidate.aadharNo}
              onChange={this.handleChange}
              helperText="can't edit the candidate"
              className={useStyles.textField}
              margin="normal"
              fullWidth
              disabled
            />
            <InputLabel id="modeOfInterview">Mode Of Interview</InputLabel>
            <Select
              labelId="demo-simple-select-error-label"
              id="modeOfInterview"
              value={this.state.interview.modeOfInterview}
              onChange={this.handleChange}
              fullWidth
              variant="filled"
              margin="normal"
              name="modeOfInterview"
            >
              <MenuItem value="null" disabled>
                <em>Mode Of Interview</em>
              </MenuItem>
              <MenuItem value="technical">Technical</MenuItem>
              <MenuItem value="HR">HR</MenuItem>
              <MenuItem value="Manager">Manager</MenuItem>
            </Select>
            <TextField
              id="filled-secondary"
              label="Date"
              variant="outlined"
              color="secondary"
              fullWidth
              name="date"
              value={this.state.interview.date}
              onChange={this.handleChange}
              helperText="edit the date"
              className={useStyles.textField}
              margin="normal"
            />
            <TextField
              id="outlined-secondary"
              label="Location"
              variant="outlined"
              color="secondary"
              margin="normal"
              name="location"
              value={this.state.interview.location}
              onChange={this.handleChange}
              fullWidth
              helperText="edit the location"
              className={useStyles.textField}
            />

            <TextField
              id="outlined-secondary"
              label="Recommendation"
              variant="outlined"
              color="secondary"
              margin="normal"
              name="recommendation"
              value={this.state.interview.recommendation}
              onChange={this.handleChange}
              helperText="edit the recommendation"
              fullWidth
              className={useStyles.textField}
            />

            <TextField
              id="outlined-secondary"
              label="InterviewPanel"
              variant="outlined"
              color="secondary"
              margin="normal"
              name="interviewPanel"
              value={this.state.interview.interviewPanel}
              onChange={this.handleChange}
              helperText="edit the ref"
              fullWidth
              className={useStyles.textField}
            />
            <TextField
              id="outlined-secondary"
              label="reference"
              variant="outlined"
              color="secondary"
              margin="normal"
              name="reference"
              value={this.state.interview.reference}
              onChange={this.handleChange}
              helperText="edit the ref"
              fullWidth
              className={useStyles.textField}
            />
            <Button
              type="submit"
              variant="outlined"
              color="secondary"
              size="large"
              startIcon={<SaveIcon />}
            >
              Save
            </Button>
          </form>
        </div>
      </div>
    );
  }
}
export default InterviewEdit;
