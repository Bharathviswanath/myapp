import React, { Component } from "react";
import axios from "axios";
import swal from "sweetalert";

export class Candidate extends Component {
  constructor() {
    super();
    this.state = {
      aadharNo: "",
      firstName: "",
      surName: "",
      phoneNumber: "",
      alternateNumber: "",
      email: "",
      tEx: "",
      rEx: "",
      primarySkills: "",
      secondarySkills: "",
      qualification: "",
      additionalQualification: "",
      expectedCTC: "",
      currentCTC: "",
      positionApplied: "",
      currentAddress: "",
      permanentAddress: "",
      gender: "",
      date: ""
    };
  }

  onChange = e => {
    const state = this.state;
    state[e.target.name] = e.target.value;
    this.setState(state);
  };
  handleClear = () => {
    this.setState({
      cand: this.state
    });
  };

  onSubmit = e => {
    e.preventDefault();

    const {
      aadharNo,
      firstName,
      surName,
      phoneNumber,
      alternateNumber,
      email,
      tEx,
      rEx,
      primarySkills,
      secondarySkills,
      qualification,
      additionalQualification,
      expectedCTC,
      currentCTC,
      positionApplied,
      currentAddress,
      permanentAddress,
      gender,
      date
    } = this.state;

    swal({
      title: "Confirmation to Add????",
      text: "Agree wheather the give statement is true",
      icon: "warning",
      buttons: true
    }).then(del => {
      if (del) {
        axios
          .post("http://localhost:8080/candidate", {
            aadharNo,
            firstName,
            surName,
            phoneNumber,
            alternateNumber,
            email,
            tEx,
            rEx,
            primarySkills,
            secondarySkills,
            qualification,
            additionalQualification,
            expectedCTC,
            currentCTC,
            positionApplied,
            currentAddress,
            permanentAddress,
            gender,
            date
          })
          .then(result => {
            if (result.status === 200) {
              swal({
                title: "Successfull Added",
                text: "you have successfully Added",

                icon: "success",
                buttons: true
              }).then(res => {
                window.location.reload();
              });
            }
          });
      } else {
        swal({
          title: "Adding request declined",
          icon: "info"
        });
      }
    });
    this.props.history.push("/");
    this.props.history.push("/Candidate");
  };

  ageHandler = event => {
    event.preventDefault();
    var candiDobYear = Number(event.target.value.substr(0, 4));
    var currentYear = new Date().getFullYear();
    var candAge = currentYear - candiDobYear;
    this.setState({
      dob: event.target.value,
      age: candAge
    });
  };

  render() {
    const {
      aadharNo,
      firstName,
      surName,
      phoneNumber,
      alternateNumber,
      email,
      tEx,
      rEx,
      primarySkills,
      secondarySkills,
      qualification,
      additionalQualification,
      expectedCTC,
      currentCTC,
      positionApplied,
      currentAddress,
      permanentAddress,
      gender,
      date
    } = this.state;
    const startday = new Date();
    startday.setFullYear(startday.getFullYear() - 60);
    const startmonth =
      startday.getMonth() + 1 < 10
        ? "0" + (startday.getMonth() + 1).toString()
        : startday.getMonth().toString();
    const startString =
      startday.getFullYear().toString() +
      "-" +
      startmonth +
      "-" +
      startday.getDate().toString();
    const endday = new Date();
    endday.setFullYear(endday.getFullYear() - 18);
    const endmonth =
      endday.getMonth() + 1 < 10
        ? "0" + (endday.getMonth() + 1).toString()
        : endday.getMonth().toString();
    const endString =
      endday.getFullYear().toString() +
      "-" +
      endmonth +
      "-" +
      endday.getDate().toString();
    return (
      <div class="row" className="bg">
        <div>
          <body>
            <div class="col-md-12">
              <div class="col-md-12">
                <h2>Candidate Application Form</h2>
              </div>
            </div>
          </body>

          <form onSubmit={this.onSubmit} onChange={this.handleClear}>
            <div>
              <div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="col-md-6">
                      <div class="row ">
                        <div class="col-md-3">Aadhar Number</div>
                        <div class="col-md-3">
                          <input
                            type="text"
                            // class="form-control"
                            name="aadharNo"
                            value={aadharNo}
                            onChange={this.onChange}
                            placeholder="Enter 12digit number"
                            pattern="[0-9]{12}"
                            maxLength="12"
                          />
                          <div className="errorMsg"></div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">First Name</div>
                        <div class="col-md-3">
                          <input
                            type="text"
                            //  class="form-control"
                            name="firstName"
                            value={firstName}
                            pattern="[A-Za-z]{1,32}"
                            maxlength="32"
                            required
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>
                      <br></br>
                      <div class="row">
                        <div class="col-md-3">Last Name</div>
                        <div class="col-md-3">
                          <input
                            type="text"
                            // class="form-control"
                            name="surName"
                            required
                            value={surName}
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>
                      <br></br>
                      <div class="row">
                        <div class="col-md-3">Phone Number</div>
                        <div class="col-md-3">
                          <input
                            type="text"
                            // class="form-control"
                            name="phoneNumber"
                            pattern="[0-9]{10}"
                            required
                            maxLength="10"
                            value={phoneNumber}
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>
                      <br></br>
                      <div class="row">
                        <div class="col-md-3">Alternate Number</div>
                        <div class="col-md-3">
                          <input
                            type="text"
                            // class="form-control"
                            name="alternateNumber"
                            value={alternateNumber}
                            maxLength="10"
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">Email Id </div>
                        <div class="col-md-3">
                          <input
                            type="text"
                            // class="form-control"
                            name="email"
                            value={email}
                            onChange={this.onChange}
                            placeholder="abc@mail.com"
                            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                            required
                          ></input>
                        </div>
                      </div>
                      <br></br>
                      <div class="row">
                        <div class="col-md-3">Total Experience</div>
                        <div class="col-md-6">
                          <input
                            type="text"
                            // class="form-control"
                            name="tEx"
                            value={tEx}
                            required
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>

                      <div class="row">
                        <div class="col-md-3">Relevant Experience</div>
                        <div class="col-md-6">
                          <input
                            type="text"
                            // class="form-control"
                            name="rEx"
                            value={rEx}
                            required
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-3">Primary Skills</div>
                        <div class="col-md-6">
                          <input
                            type="text"
                            // class="form-control"
                            name="primarySkills"
                            value={primarySkills}
                            required
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>
                      <br></br>
                      <div class="row">
                        <div class="col-md-3">Secondary Skills</div>
                        <div class="col-md-6">
                          <input
                            type="text"
                            // class="form-control"
                            name="secondarySkills"
                            value={secondarySkills}
                            onChange={this.onChange}
                          ></input>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-6">Educational Qualification</div>
                          <div class="col-md-3">
                            <input
                              type="text"
                              // class="form-control"
                              name="qualification"
                              value={qualification}
                              onChange={this.onChange}
                              required
                            ></input>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">Additional Education</div>
                          <div class="col-md-3">
                            <input
                              type="text"
                              // class="form-control"
                              name="additionalQualification"
                              value={additionalQualification}
                              onChange={this.onChange}
                            ></input>
                          </div>
                        </div>
                        <div class="row ">
                          <div class="col-md-6"> Expected CTC</div>
                          <div class="col-md-3">
                            <input
                              type="text"
                              // class="form-control"
                              name="expectedCTC"
                              value={expectedCTC}
                              required
                              placeholder="enter in numbers"
                              onChange={this.onChange}
                            ></input>
                          </div>
                        </div>
                        <br></br>
                        <div class="row">
                          <div class="col-md-6">Current CTC</div>
                          <div class="col-md-3">
                            <input
                              type="text"
                              // class="form-control"
                              name="currentCTC"
                              value={currentCTC}
                              required
                              placeholder="enter in numbers"
                              onChange={this.onChange}
                            ></input>
                          </div>
                        </div>
                        <br></br>
                        <div class="row">
                          <div class="col-md-6">Position Applied</div>
                          <div class="col-md-3">
                            <input
                              type="text"
                              //  class="form-control"
                              name="positionApplied"
                              value={positionApplied}
                              required
                              onChange={this.onChange}
                            ></input>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">Current Address</div>
                          <div class="col-md-3">
                            <input
                              type="text"
                              // class="form-control"
                              name="currentAddress"
                              value={currentAddress}
                              required
                              onChange={this.onChange}
                              placeholder="Enter current address"
                            ></input>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">Residential Address</div>
                          <div class="col-md-3">
                            <input
                              type="text"
                              //  class="form-control"
                              name="permanentAddress"
                              value={permanentAddress}
                              required
                              onChange={this.onChange}
                              placeholder="Enter residential address"
                            ></input>
                          </div>
                        </div>

                        <div>
                          <div class="row">
                            <div class="col-md-6">Gender</div>

                            <div class="col-md-6">
                              <input
                                type="radio"
                                name="gender"
                                value={gender}
                                onChange={this.onChange}
                                required
                                id="gender"
                                name="groupOfMaterialRadios"
                              ></input>

                              <label for="gender">Male</label>
                              <input
                                type="radio"
                                // class="form-control"
                                name="gender"
                                value={gender}
                                onChange={this.onChange}
                                class="form-check-input"
                                id="gender"
                                name="groupOfMaterialRadios"
                              ></input>
                              <label for="gender">Female</label>
                            </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-6">DOB</div>
                          <div class="col-md-6">
                            <input
                              type="date"
                              // class="form-control"
                              name="date"
                              min={startString}
                              max={endString}
                              value={this.date}
                              required
                              onChange={this.onChange}
                            ></input>
                            <br></br>
                            <br></br>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <button class="btn btn-primary"> Submit</button>
                      </div>

                      <button class="btn btn-danger" type="reset" value="Reset">
                        Clear
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
