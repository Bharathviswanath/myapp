import React, { Component } from "react";

import axios from "axios";
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

export class Interview extends Component {
  candidate = {
    aadharNo: 0,

    firstName: "",
    surname: "",
    phoneNumber: "",
    alternateNumber: "",
    email: "",
    tEx: "",
    rEx: "",
    primarySkills: "",
    secondarySkills: "",
    qualification: "",
    additionalQualification: "",
    expectedCTC: 0,
    currentCTC: 0,
    positionApplied: "",
    currentAddress: "",
    permanentAddress: "",
    gender: "",
    date: ""
  };

  interview = {
    candidate: this.candidate,
    id: "",
    modeOfInterview: "",
    location: "",
    recommendation: "",
    interviewPanel: "",
    reference: "",
    date: ""
  };
  state = {
    interview: this.interview,
    candidateaadhar: []
  };
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.handleClear = this.handleClear.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleClear = () => {
    this.setState({
      interview: this.interview
    });
  };

  handleSubmit(event) {
    event.preventDefault();
    let interview = this.state.interview;
    console.log("submit", interview);

    if (window.confirm("Do you want to submit the info???")) {
      axios
        .post("http://localhost:8080/interview", interview)
        .then(res => {
          console.log(this.candidate);
        })
        .catch(res => console.log(res));
    } else {
    }
  }
  handleClear = event => {
    this.setState({
      interview: this.interview
    });
  };
  change() {
    if (true) {
      return;
    }
  }

  handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    let inter = { ...this.state.interview };
    if (name == "candidate") {
      inter.candidate.aadharNo = value;
    } else {
      console.log("others");
      inter[name] = value;
    }

    this.setState({
      ...this.state,
      interview: inter
    });
  };

  render() {
    let ar = this.state.candidateaadhar;
    let newar = ar.map(datum => {
      return (
        <option key={datum.aadharNo} value={datum.aadharNo}>
          {datum.aadharNo + " : " + datum.firstName + " " + datum.lastName}
        </option>
      );
    });

    const today = new Date();
    const month =
      today.getMonth() + 1 < 10
        ? "0" + (today.getMonth() + 1).toString()
        : today.getMonth().toString();
    const todayString =
      today.getFullYear().toString() +
      "-" +
      month +
      "-" +
      today.getDate().toString();

    return (
      <div class="col-md-12" className="bg">
        <div className="col-md-6">
          <img
            src="https://www.teamworkandleadership.com/wp-content/uploads/2015/03/funny-job-interview-videos.jpg"
            class="float-right"
            alt="Responsive image"
            height="100%"
          ></img>
        </div>

        <form onSubmit={this.handleSubmit}>
          <form onSubmit={this.handleClear}>
            <div className="col-md-6">
              <div class="row">
                <div class="col-md-6">CandidateNo</div>

                <input
                  type="text"
                  name="candidate"
                  value={this.state.interview.candidate.aadharNo}
                  onChange={this.handleChange}
                  placeholder="Enter aadhar number"
                  required
                  pattern="[0-9]{12}"
                  maxLength="12"
                ></input>
              </div>
              <br></br>
              <div class="row">
                <div class="col-md-3">Mode Of Interview</div>
                <Select
                  labelId="demo-simple-select-error-label"
                  id="modeOfInterview"
                  value={this.state.interview.modeOfInterview}
                  onChange={this.handleChange}
                  variant="filled"
                  margin="normal"
                  name="modeOfInterview"
                >
                  <MenuItem value="null" disabled>
                    <em>Mode Of Interview</em>
                  </MenuItem>
                  <MenuItem value="facetoface" onClick={this.state.enable}>
                    FaceToFace
                  </MenuItem>

                  <MenuItem value="telephonic">Tele-Phonic</MenuItem>
                  <MenuItem value="videocall ">Video Call</MenuItem>
                </Select>
                <div class="col-md-3">
                  <input type="text"></input>
                </div>
              </div>

              <br></br>
              <div class="row">
                <div class="col-md-6">Interview Date</div>
                <input
                  type="date"
                  name="date"
                  min={todayString}
                  value={this.state.interview.date}
                  onChange={this.handleChange}
                  required
                ></input>
              </div>
              <br></br>
              <div class="row">
                <div class="col-md-6">Interview Location</div>
                <textarea
                  name="location"
                  type="location"
                  rows="4"
                  cols="30"
                  value={this.state.interview.location}
                  onChange={this.handleChange}
                  required
                ></textarea>
              </div>
              <br></br>
              <div class="row">
                <div class="col-md-6">Recommendation</div>
                <input
                  type="text"
                  name="recommendation"
                  value={this.state.interview.recommendation}
                  required
                  onChange={this.handleChange}
                ></input>
              </div>
              <br></br>
              <div class="row">
                <div class="col-md-6">Interview Panel</div>
                <div class="form-check">
                  <input
                    type="checkbox"
                    class="form-check-input"
                    id="materialUnchecked"
                    value={this.state.interview.interviewPanel}
                    required
                    onChange={this.handleChange}
                  ></input>
                  <br></br>
                  <label class="form-check-label">Technical</label>
                  <br></br>
                  <input
                    type="checkbox"
                    class="form-check-input"
                    id="materialUnchecked"
                    value={this.state.interview.interviewPanel}
                    onChange={this.handleChange}
                  ></input>
                  <br></br>
                  <label class="form-check-label">HR</label>
                  <br></br>
                  <input
                    type="checkbox"
                    class="form-check-input"
                    id="materialUnchecked"
                    value={this.state.interview.interviewPanel}
                    onChange={this.handleChange}
                  ></input>
                  <br></br>
                  <label class="form-check-label">Manager Round</label>
                </div>
              </div>
              <br></br>
              <div class="row">
                <div class="col-md-6">Reference</div>
                <input
                  type="text"
                  name="reference"
                  placeholder="EmpID"
                  value={this.state.interview.reference}
                  onChange={this.handleChange}
                ></input>
              </div>
              <br></br>
              <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-2">
                  <button type="submit" class="btn btn-success" value="submit">
                    Submit
                  </button>
                </div>

                <div class="col-md-2">
                  <button type="button" class="btn btn-danger">
                    Cancel
                  </button>
                </div>
              </div>
            </div>
          </form>
        </form>
      </div>
    );
  }
}
